<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->unsignedBigInteger('id_postulant')-> unique()->nullable();
            $table->string('nama');
            $table->string('kode_karyawan')-> unique();
            $table->string('password');
            $table->string('level');
            $table->unsignedBigInteger('id_jabatan');
            $table->unsignedBigInteger('id_grade');
            $table->unsignedBigInteger('id_corporate');
            $table->unsignedBigInteger('id_devisi');
            $table->datetime('join_date');
            $table->char('status', 15);
            $table->char('email_corporate', 50);
            $table->char('bank_account', 25);
            $table->char('rekening_number', 25);
            $table->char('account_name', 25);
            $table->char('nama_atasan', 100);
            $table->Integer('jatah_cuti');
            $table->datetime('first_contract');
            $table->datetime('start_addendum1');
            $table->datetime('end_addendum1');
            $table->datetime('start_addendum2');
            $table->datetime('end_addendum2');
            $table->datetime('end_contract');
            $table->char('alasan_berhenti', 25);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_jabatan')
                  ->references('id')
                  ->on('positions')
                  ->onDelete('cascade');

            $table->foreign('id_grade')
                  ->references('id')
                  ->on('grades')
                  ->onDelete('cascade');
            
            $table->foreign('id_corporate')
                  ->references('id')
                  ->on('corporate_groups')
                  ->onDelete('cascade');
            $table->foreign('id_devisi')
                  ->references('id')
                  ->on('divisions')
                  ->onDelete('cascade');
             $table->foreign('id_postulant')
                  ->references('id')
                  ->on('postulants')
                  ->onDelete('cascade');
            
            
         

             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
