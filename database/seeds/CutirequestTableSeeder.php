<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CutirequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuti_requests')->insert([
            [
                'tanggal_request' => '2019-12-26 00:00:00',
                'id_karyawan' => '1',
                'nama_atasan' => 'Dika',
                'id_cuti' => '1',
                'tanggal_cuti' => '2019-12-26 00:00:00',
                'lama_cuti' => '2',
                'alasan' => 'llefefef',
                'cuti_terpakai' => '2',
                'keputusan_direktur' => 'llefefef',
                'catatan_direktur' => 'llefefef',
                'keputusan_hrd' => 'llefefef',
                'catatan_hrd' => 'llefefef',
                'keputusan_atasan' => 'llefefef',
                'catatan_atasan' => 'llefefef',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],
                [
                'tanggal_request' => '2019-12-25 00:00:00',
                'id_karyawan' => '1',
                'nama_atasan' => 'Dika',
                'id_cuti' => '1',
                'tanggal_cuti' => '2019-12-26 00:00:00',
                'lama_cuti' => '2',
                'alasan' => 'llefefef',
                'cuti_terpakai' => '2',
                'keputusan_direktur' => 'llefefef',
                'catatan_direktur' => 'llefefef',
                'keputusan_hrd' => 'llefefef',
                'catatan_hrd' => 'llefefef',
                'keputusan_atasan' => 'llefefef',
                'catatan_atasan' => 'llefefef',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],

            ]);
    }
}
