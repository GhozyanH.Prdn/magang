<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->insert([
            [
            'id_postulant' => '1',
            'nama_organisasi' => 'Pramuka Unipma',
            'jabatan' => 'Kepala Gugus',
            'periode' => '2000',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_postulant' => '1',
            'nama_organisasi' => 'Osis Sma Bati',
            'jabatan' => 'Ketua',
            'periode' => '2000',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
