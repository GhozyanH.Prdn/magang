<?php

return [

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],



    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'employees',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'employees',
            'hash' => false,
        ],

        'personal' => [
            'driver' => 'session',
            'provider' => 'personals',
        ],
    ],


    'providers' => [
        'employees' => [
            'driver' => 'eloquent',
            'model' => App\Employee::class,
        ],

        'personals' => [
            'driver' => 'eloquent',
            'model' => App\Postulant::class,
        ],


    ],



    'passwords' => [
        'employees' => [
            'provider' => 'employees',
            'table' => 'password_resets',
            'expire' => 60,
            'throttle' => 60,
        ],

        'personals' => [
            'provider' => 'personals',
            'table' => 'password_resets',
            'expire' => 60,
            'throttle' => 60,
        ],
    ],


    'password_timeout' => 10800,

];
