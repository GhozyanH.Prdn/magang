<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>PT Davinti Indonesia</title>

    <!-- Bootstrap -->
    <link href="/material/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/material/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/material/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/material/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Datatables -->

    <link href="/material/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


    <!-- bootstrap-progressbar -->
    <link href="/material/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/material/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/material/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/material/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col menu_fixed">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Davinti Indonesia</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="/material/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>{{ date('l, d-m-Y') }}</span>
              <h2>{{auth()->user()->nama}}</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>{{auth()->user()->level}}</h3>
              <ul class="nav side-menu">

                @if(auth()->user()->level=='Admin')
                <li><a href="{{ url('/admin') }}"><i class="fa fa-home"></i> Home</a>
                </li>
                @endif

                @if(auth()->user()->level=='Admin')
                <li><a><i class="fa fa-users"></i> Karyawan <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/employees') }}">Data Karyawan</a></li>
                    <li><a href="{{ url('/karyawanresign') }}">Karyawan Resign</a></li>
                  </ul>
                </li>
                @endif

                @if(auth()->user()->level=='Pegawai' and 'Atasan')
                <li><a href="{{ url('/data') }}"><i class="fa fa-user"></i> Data Diri</a>
                </li>
                @endif

                @if(auth()->user()->level=='Admin')
                <li><a href="{{ url('/assets') }}"><i class="fa fa-desktop"></i> Aset</a>
                </li>
                @endif
                @if(auth()->user()->level=='Pegawai' and 'Atasan')
                <li><a href="{{ url('/data') }}"><i class="fa fa-money"></i> Gaji</a>
                </li>
                @endif
                <!-- @if(auth()->user()->level=='Admin')
                <li><a><i class="fa fa-money"></i> Gaji <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/wdwdwd') }}">Data Gaji</a></li>
                    <li><a href="{{ url('/wdwd') }}">Input Gaji</a></li>
                  </ul>
                </li>
                @endif -->
                @if(auth()->user()->level=='Admin')
                <li><a><i class="fa fa-user-times"></i> Cuti <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/cuti') }}">Data Pengajuan Cuti</a></li>
                    <li><a href="{{ url('/category') }}">Kategori Cuti</a></li>
                  </ul>
                </li>
                @endif
                @if(auth()->user()->level=='Atasan')
                <li><a><i class="fa fa-user-times"></i> Cuti <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/cuti_atasan') }}"> Pengajuan Cuti</a></li>
                    <li><a href="{{ url('/persetujuan_atasan') }}">Form Persetujuan Cuti</a></li>
                  </ul>
                </li>
                @endif
                @if(auth()->user()->level=='Direktur' )
                <li><a href="{{ url('/persetujuan_direktur') }}"><i class="fa fa-user-times"></i> Cuti </a>
                </li>
                @endif
                
                @if(auth()->user()->level=='Pegawai' )
                <li><a href="{{ url('/cutipegawai') }}"><i class="fa fa-user-times"></i> Cuti </a>
                </li>
                @endif
                @if(auth()->user()->level=='Admin')
                <li><a><i class="fa fa-table"></i> Interview <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/interview') }}">Data Interview</a></li>
                    <li><a href="{{ url('/lalla') }}">Data Tes Personal</a></li>
                  </ul>
                </li>
                @endif
                @if(auth()->user()->level=='Admin')
                <li><a><i class="fa fa-edit"></i> Kelola Manajemen <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/jabatan') }}">Jabatan</a></li>
                    <li><a href="{{ url('/divisi') }}">Devisi</a></li>
                    <li><a href="{{ url('/grade') }}">Grade</a></li>
                    <li><a href="{{ url('/corporate') }}">Corporate Group</a></li>
                  </ul>
                </li>
                @endif
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="/logout">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
          <div class="nav_menu">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <nav class="nav navbar-nav">
              <ul class=" navbar-right">
                <li class="nav-item dropdown open" style="padding-left: 15px;">
                  <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <img src="/material/images/img.jpg" alt="">{{auth()->user()->nama}}
                  </a>
                  <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item"  href="javascript:;"> Profile</a>
                      <a class="dropdown-item"  href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                  <a class="dropdown-item"  href="javascript:;">Help</a>
                    <a class="dropdown-item"  href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </div>
                </li>

                <li role="presentation" class="nav-item dropdown open">
                  <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <span class="image"><img src="/material/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <span class="image"><img src="/material/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <span class="image"><img src="/material/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <span class="image"><img src="/material/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <div class="text-center">
                        <a class="dropdown-item">
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="/material/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/material/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="/material/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/material/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/material/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/material/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/material/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/material/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/material/vendors/skycons/skycons.js"></script>
    <!-- Datatables -->
    <script src="/material/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/material/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/material/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/material/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/material/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/material/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/material/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/material/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/material/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/material/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Flot -->
    <script src="/material/vendors/Flot/jquery.flot.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/material/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/material/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/material/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/material/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/material/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/material/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/material/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/material/vendors/moment/min/moment.min.js"></script>
    <script src="/material/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/material/build/js/custom.min.js"></script>
    @yield('script')
    <script> 
      $('#modalEditDivisi').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idDiv = button.data('myiddiv')
        var kodeDiv = button.data('mykodediv')
        var namaDiv = button.data('mynamadiv')
        var modal = $(this)

<<<<<<< HEAD
<!-- Edit Divisi -->
    <script> 
      $('#modalEditDivisi').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idDiv = button.data('myiddiv')
        var kodeDiv = button.data('mykodediv')
        var namaDiv = button.data('mynamadiv')
        var modal = $(this)

        modal.find('#editDivisi #iddiv').val(idDiv);
        modal.find('#editDivisi #kodediv').val(kodeDiv);
        modal.find('#editDivisi #namadiv').val(namaDiv);
=======
        modal.find('#editDivisi #id').val(idDiv);
        modal.find('#editDivisi #kode').val(kodeDiv);
        modal.find('#editDivisi #nama').val(namaDiv);
>>>>>>> 553790c203b2165b28abfb9ba75c7cd0d35f0ace
      })
    </script>

<!-- Edit Corporate -->
    <script> 
      $('#modalEditCorporate').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idCor = button.data('myidcor')
        var kodeCor = button.data('mykodecor')
        var namaCor = button.data('mynamacor')
        var modal = $(this)

        modal.find('#editCorporate #idcor').val(idCor);
        modal.find('#editCorporate #kodecor').val(kodeCor);
        modal.find('#editCorporate #namacor').val(namaCor);
      })
    </script>

<!-- Edit Jabatan -->
    <script> 
      $('#modalEditJabatan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idJab = button.data('myidjab')
        var kodeJab = button.data('mykodejab')
        var namaJab = button.data('mynamajab')
        var modal = $(this)

        modal.find('#editJabatan #idjab').val(idJab);
        modal.find('#editJabatan #kodejab').val(kodeJab);
        modal.find('#editJabatan #namajab').val(namaJab);
      })
    </script>

<!-- Edit Grade -->
    <script> 
      $('#modalEditGrade').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idGrd = button.data('myidgrd')
        var kodeGrd = button.data('mykodegrd')
        var namaGrd = button.data('mynamagrd')
        var modal = $(this)

        modal.find('#editGrade #idgrd').val(idGrd);
        modal.find('#editGrade #kodegrd').val(kodeGrd);
        modal.find('#editGrade #namagrd').val(namaGrd);
      })
    </script>
<<<<<<< HEAD

<!-- Edit Category -->
<script> 
      $('#modalEditCategory').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idCc = button.data('myidcc')
        var kodeCc = button.data('mykodecc')
        var jenisCc = button.data('myjeniscc')
        var ketCc = button.data('myketcc')
        var valueCc = button.data('myvaluecc')
        var modal = $(this)

        modal.find('#editCategory #idcc').val(idCc);
        modal.find('#editCategory #kodecc').val(kodeCc);
        modal.find('#editCategory #jeniscc').val(jenisCc);
        modal.find('#editCategory #ketcc').val(ketCc);
        modal.find('#editCategory #valuecc').val(valueCc);
      })
    </script>
    
<!-- Edit Keputusan -->
<script> 
      $('#modalKeputusan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idIntv = button.data('myidintv')
        var namaIntv = button.data('mynamaintv')
        var posisiIntv = button.data('myposisiintv')        
        var keputusanIntv = button.data('mykeputusanintv')
        var catatanIntv = button.data('mycatatanintv')
        var alasanIntv = button.data('myalasanintv')
        var modal = $(this)

        modal.find('#editKeputusan #idintv').val(idIntv);
        modal.find('#editKeputusan #namaintv').val(namaIntv);
        modal.find('#editKeputusan #posisiintv').val(posisiIntv);
        modal.find('#editKeputusan #keputusanintv').val(keputusanIntv);
        modal.find('#editKeputusan #catatanintv').val(catatanIntv);
        modal.find('#editKeputusan #alasanintv').val(alasanIntv);
      })
    </script>   

<!-- Edit Catatan -->
<script> 
      $('#modalCatatan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idIntv = button.data('myidintv')
        var namaIntv = button.data('mynamaintv')
        var posisiIntv = button.data('myposisiintv')
        var catatanIntv = button.data('mycatatanintv')
        var alasanIntv = button.data('myalasanintv')
        var modal = $(this)

        modal.find('#editCatatan #idintv').val(idIntv);
        modal.find('#editCatatan #namaintv').val(namaIntv);
        modal.find('#editCatatan #posisiintv').val(posisiIntv);
        modal.find('#editCatatan #catatanintv').val(catatanIntv);
        modal.find('#editCatatan #alasanintv').val(alasanIntv);
      })
    </script>   

<!-- Edit Keputusan -->
<script> 
      $('#modalKeputusan2').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idIntv = button.data('myidintv')
        var namaIntv = button.data('mynamaintv')
        var posisiIntv = button.data('myposisiintv')        
        var keputusanIntv = button.data('mykeputusanintv')
        var catatanIntv = button.data('mycatatanintv')
        var alasanIntv = button.data('myalasanintv')
        var modal = $(this)

        modal.find('#editKeputusan2 #idintv').val(idIntv);
        modal.find('#editKeputusan2 #namaintv').val(namaIntv);
        modal.find('#editKeputusan2 #posisiintv').val(posisiIntv);
        modal.find('#editKeputusan2 #keputusanintv').val(keputusanIntv);
        modal.find('#editKeputusan2 #catatanintv').val(catatanIntv);
        modal.find('#editKeputusan2 #alasanintv').val(alasanIntv);
      })
    </script>
=======
   
>>>>>>> 553790c203b2165b28abfb9ba75c7cd0d35f0ace

  </body>
</html>
