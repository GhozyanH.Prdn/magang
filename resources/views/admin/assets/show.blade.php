@extends('layouts.master')

@section('content')
<div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Invoice Design <small>Sample user invoice design</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="  invoice-header">
                          <h1>
                              <img src="{{url('images/aset/' . $asset -> gambar)}}" width="100px;" height="100px;" alt="gambar">
                            <i ></i> {{$asset -> nama_aset}}

                        </h1>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Kategori :
                          <address>
                                          <strong>{{$asset -> kategori}}</strong>
                                          <br>Kode Aset&emsp;&emsp;:<strong>&emsp;{{$asset -> kode_aset}}</strong>
                                          <br>Tanggal Beli&emsp;: <strong>&emsp;{{$asset -> tanggal_beli}}</strong>

                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          PIC Sekerang :
                          <address>
                            @foreach ($users as $user)
                                @if($user->nama) <strong>{{$user->nama}}</strong>
                                  @else <strong>kosong</strong>
                                  @endif
                            @endforeach


                                      </address>
                        </div>

                      </div>
                      <!-- /.row -->

                      <div class="x_content">
                          <div class="row">
                              <div class="col-sm-12">
                                <div class="card-box table-responsive">
                        <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                            <tr>
                              <th>No </th>
                              <th >Kode Peminjaman</th>
                              <th>Tanggal Peminjaman</th>
                              <th>Tanggal Kembali</th>
                              <th>Nama PIC</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($tes as $at)
                              <tr>
                                  <td>{{$loop -> iteration}}</td>
                                  <td>{{$at -> kode_peminjaman}}</td>
                                  <td>{{$at -> tanggal_pinjam}}</td>
                                  <td>{{$at -> tanggal_kembali}}</td>
                                  <td>{{$at -> nama}}</td>
                            </tr>
                          @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  </div>


                      <!-- this row will not appear when printing -->
                    </section>
                  </div>
                </div>
              </div>
            </div>
@endsection
