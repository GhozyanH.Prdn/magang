@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Edit Assets <small>By : {{auth()->user()->level}} ({{auth()->user()->nama}})</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Aplikasi Aset  <small>PT Davinti Indonesia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Settings 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form action="/assets/{{$asset->id}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
          @method('patch')
          @csrf
          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Kategori <span class="required">*</span>
            </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select name="kategori" value="{{$asset -> kategori}}"  class="form-control">
                            <option>Choose option</option>
                            <option>Elektronik</option>
                            <option>Alat Tulis Kantor</option>
                          </select>
                        </div>
                      </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Kode Aset <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="kode_aset" value="{{$asset -> kode_aset}}" readonly class="form-control" placeholder="Automatic Generate">
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nama Aset</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$asset -> nama_aset}}" type="text" name="nama_aset">
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tanggal Beli</label>
            <div class="col-md-6 col-sm-6 ">
              <input  class="form-control" value="{{$asset -> tanggal_beli}}" type="date" name="tanggal_beli">
            </div>
          </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align">Foto Aset <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input   type="file" value="{{$asset -> gambar}}" name="image">
              <input class="date-picker form-control" type="text" value="{{$asset -> gambar}}"   placeholder="No Image selected">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="item form-group ">
            <div class="col-md-6 col-sm-6 offset-md-3 ">
              <button type="reset" class="btn btn-round btn-primary">Reset</button>
              <button type="submit" class="btn btn-round btn-success">Save</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

@endsection
