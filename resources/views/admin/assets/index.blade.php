@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Users <small>Some examples to get you started</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Aplikasi Aset  <small>PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
          <a href="assets/create" class="btn btn-custon-four btn-default"><i class="fa fa-reply"></i> Tambah Data</a>
          <a href="assets/trash" class="btn btn-custon-four btn-warning"><i class="fa fa-refresh fa-spin  fa-fw"></i> Data Trash</a>
          
          @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Dihapus.
          </div>
          @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No Aset</th>
            <th>Kategori</th>
            <th>Kode Aset</th>
            <th>Nama Aset</th>
            <th>Tanggal Beli</th>
            <th>Gambar</th>
            <th>Action</th>
            <th>Aset</th>
          </tr>
        </thead>


        <tbody>
          @foreach ($assets as $v => $ast )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$ast -> kategori}}</td>
            <td>{{$ast -> kode_aset}}</td>
            <td>{{$ast -> nama_aset}}</td>
            <td>{{$ast -> tanggal_beli}}</td>
            <td><img src="{{url('images/aset/' . $ast -> gambar)}}" width="100px;" height="100px;" alt="gambar"></td>
            <td>
                <form action="/assets/{{$ast -> id}}" method="post" class="d-inline">
                @method('delete')
                @csrf
                <a href="/assets/{{$ast->id}}" class="btn btn-custon-four btn-success"><i class="fa fa-info-circle" style="font-size:15px"></i></a>
                <a href="/assets/{{$ast->id}}/edit" class="btn btn-custon-four btn-warning"><i class="fa fa-pencil-square-o" style="font-size:15px"></i></a>
                <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" style="font-size:15px"></i></button>
                </form>
            </td>
            <td>

                @if($ast->id)<a href="/assets/{{$ast->id}}/transaksi" class="btn btn-custon-four btn-success">Pengembalian</a>
                @else <a href="/assets/{{$ast->id}}/transaksi" class="btn btn-custon-four btn-warning">Penyerahan</a> @endif
            </td>

          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>

@endsection
