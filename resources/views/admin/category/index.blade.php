@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>PT Davinti Indonesia</h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Kategori Cuti</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
          </div>
        </li> 
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li> -->
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
          <a href="" class="btn btn-custon-four btn-primary" data-toggle="modal" data-target="#modalTambahCategory"><i class="fa fa-reply"></i> Tambah Kategori Cuti</a>
          <a href="category/trash" class="btn btn-custon-four btn-warning"><i class="fa fa-refresh fa-spin fa-fw"></i> Dapatkan Data Terhapus</a>
          @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>SUKSES !!!</strong> Data Kategori Cuti Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>SUKSES !!!</strong> Data Kategori Cuti Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>SUKSES !!!</strong> Data Kategori Cuti Berhasil Dihapus.
          </div>
          @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Kategori Cuti</th>
            <th>Jenis Cuti</th>
            <th>Keterangan</th>
            <th>Nilai Cuti</th>
            <th>Aksi</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($category as $v => $category )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$category -> kode_kategori_cuti}}</td>
            <td>{{$category -> jenis_cuti}}</td>
            <td>{{$category -> keterangan}}</td>
            <td>{{$category -> value}}</td>
            <td>
                <a href="/category" class="btn btn-custon-four btn-warning"
                data-myidcc="{{$category -> id}}"
                data-mykodecc="{{$category -> kode_kategori_cuti}}"
                data-myjeniscc="{{$category -> jenis_cuti}}"
                data-myketcc="{{$category -> keterangan}}"
                data-myvaluecc="{{$category -> value}}"
                data-toggle="modal" data-target="#modalEditCategory">Ubah</a>
                <form action="/category/{{$category->id}}" method="post" class="d-inline">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>


<!-- --------- MODAL --------- -->

    <!-- --------- MODAL TAMBAH --------- -->
                  <div class="modal fade" id="modalTambahCategory" tabindex="-1" role="dialog" aria-labelledby="modalTambahTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form id="formSave" method="POST" action="/category">
                        
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalTambahTitle">Tambah Grade Baru</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>                        
                          <div class="modal-body">                        
                            @csrf
                            <div class="form-group">
                                  <label for="kode">Kode Kategori Cuti</label>
                                  <input type="text" name="kodeCc" disabled class="form-control" placeholder="Automatic Generate">
                            </div>

                            <div class="form-group">
                                  <label for="nama">Jenis Cuti</label>
                                  <input type="text" class="form-control @error('jenisCc') is-invalid @enderror" name="jenisCc" placeholder="Masukan Jenis Cuti" value ="{{ old('jenisCc')}}">
                                  @error('jenisCc')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                  @enderror
                            </div>

                            <div class="form-group">
                                  <label for="nama">Keterangan</label>
                                  <input type="text" class="form-control @error('ketCc') is-invalid @enderror" name="ketCc" placeholder="Masukan Keterangan Cuti" value ="{{ old('ketCc')}}">
                                  @error('ketCc')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                  @enderror
                            </div>

                            <div class="form-group">
                                  <label for="nama">Nilai Cuti</label>                                
                                    <select name="valueCc" class="form-control @error('valueCc') is-invalid @enderror" value ="{{ old('valueCc')}}">
                                      <option>0</option>
                                      <option>0.5</option>
                                      <option>1</option>
                                    </select>
                                  @error('valueCc')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                  @enderror
                            </div>
                          </div>                        
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal" id = "closeModalTambah" >Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL TAMBAH --------- -->

    <!-- --------- MODAL EDIT --------- -->
    <div class="modal fade" id="modalEditCategory" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <form action="{{route('category.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Grade</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          
                          <div class="modal-body" id="editCategory">
                              <input type="hidden" id="idcc" name="idCc" value="">
                              
                              <div class="form-group">
                                <label for="kode">Kode Kategori Cuti</label>
                                <input type="text" disabled class="form-control" id="kodecc" name="kodeCc" value="">
                              </div>

                              <div class="form-group">
                                <label for="kode">Jenis Cuti</label>
                                <input type="text" class="form-control @error('jenisCc') is-invalid @enderror" id="jeniscc" name="jenisCc" placeholder="Masukan Jenis Kategori" value ="{{ old('jenisCc')}}">
                                @error('jenisCc')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                @enderror
                              </div>

                              <div class="form-group">
                                <label for="kode">Keterangan</label>
                                <input type="text" class="form-control @error('ketCc') is-invalid @enderror" id="ketcc" name="ketCc" placeholder="Masukan Keterangan" value ="{{ old('ketCc')}}">
                                @error('jenisCc')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                @enderror
                              </div>
                              
                              <div class="form-group">
                                  <label for="nama">Nilai Cuti</label>                                
                                    <select id="valuecc" name="valueCc" class="form-control @error('valueCc') is-invalid @enderror" value ="{{ old('valueCc')}}">
                                      <option>0</option>
                                      <option>0.5</option>
                                      <option>1</option>
                                    </select>
                                  @error('valueCc')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

@endsection