@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>PT Davinti Indonesia</h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Data Divisi Terhapus</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
          </div>
        </li> 
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li> -->
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
        <a href="/jabatan" class="btn btn-outline-secondary"> << Kembali</a>
        <a href="/jabatan/restore_all" onclick="return confirm('Apakah Anda Yakin Untuk Mengembalikan Semua Data Terhapus?')" class="btn btn-custon-four btn-primary"><i class="fa fa-refresh fa-spin fa-fw"></i> Pulihkan Semua Data</a>
        <a href="/jabatan/deleted_all" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus Secara Permanen Semua Data Ini?')" class="btn btn-custon-four btn-danger"><i class="fa fa-trash"></i> Hapus Semua Data</a>
        @if (session('status'))
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>SUKSES !!!</strong> Data Jabatan Berhasil Ditambahkan.
        </div>
        @endif

        @if (session('edit'))
        <div class="alert alert-warning alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>SUKSES !!!</strong> Data Jabatan Berhasil Diubah.
        </div>
        @endif

        @if (session('delete'))
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>SUKSES !!!</strong> Data Jabatan Berhasil Dihapus.
        </div>
        @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Jabatan</th>
            <th>Nama Jabatan</th>
            <th>Tanggal Ditambahkan</th>
            <th>Tanggal Dihapus</th>
            <th>Aksi</th>
          </tr>
        </thead>


        <tbody>
          @foreach ($positions as $position )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$position -> kode_jabatan}}</td>
            <td>{{$position -> nama_jabatan}}</td>
            <td>{{$position -> created_at}}</td>
            <td>{{$position -> deleted_at}}</td>
            <td>
              <a href="/jabatan/restore/{{$position->id}}" onclick="return confirm('Apakah Anda Yakin Untuk Mengembalikan Data Ini?')" class="btn btn-custon-four btn-success">Pulihkan Data</a>
              <a href="/jabatan/deleted_permanent/{{$position->id}}" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus Secara Permanen Data ini?')" class="btn btn-custon-four btn-danger">Hapus Data</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>

@endsection
