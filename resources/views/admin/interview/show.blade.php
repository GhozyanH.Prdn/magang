@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Karyawan Profile</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                          <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>User Report <small>Activity report</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                      </div>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li>
                                </ul>
                                <div class="clearfix"></div>
                              </div>

                              <div class="x_content">
                                <div class="col-md-2 col-sm-2 profile_left">
                                  <div class="profile_img">
                                    <div id="crop-avatar">
                                      <!-- Current avatar -->
                                      <img src="/material/images/img.jpg" alt="..." class="img-responsive avatar-view img-circle profile_img">
                                    </div>
                                  </div>
                                  <h3>{{$interview -> nama_lengkap}}</h3>

                                  <ul class="list-unstyled user_data">
                                    <li>
                                      <i class="glyphicon glyphicon-folder-open user-profile-icon"></i><h5>{{$interview -> posisi_yang_dilamar}}</h5>
                                    </li>
                                  </ul>

                                  <a class="btn btn-warning"><i class="fa fa-edit col-md-3 col-sm-3"></i> Edit Profile</a>
                                  <a href="/interview" class="btn btn-outline-secondary"><< Kembali</a>
                                  <!-- <br /> -->

                                </div>
                                <div class="col-md-10 col-sm-10 ">

                                  <div class="profile_title">
                                    <div class="col-md-6">
                                      <h2>User Activity Report</h2>
                                    </div>
                                    <!-- <div class="col-md-6">
                                      <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                                      </div>
                                    </div> -->
                                  </div>

                                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                      <li role="presentation" class="active"><a href="#tab_dataDiri" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Diri</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_dataKeluarga" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Keluarga</a>
                                      </li>
                                      <!-- <li role="presentation" class="active"><a href="#tab_pendidikan" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Pendidikan</a>
                                      </li> -->
                                      <li role="presentation" class="active"><a href="#tab_riwayatKerja" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Riwayat Kerja</a>
                                      </li>
                                      <!-- <li role="presentation" class="active"><a href="#tab_pelatihan" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Pelatihan</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_organisasi" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Organisasi</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_prestasi" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Prestasi</a>
                                      </li> -->
                                      <li role="presentation" class="active"><a href="#tab_bahasa" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Bahasa</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_referensi" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Referensi</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_riwayat" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Riwayat</a>
                                      </li>                                      
                                      <li role="presentation" class="active"><a href="#tab_lainLain" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Lainnya</a>
                                      </li>                                      
                                      <li role="presentation" class="active"><a href="#tab_testKepribadian" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Test Kepribadian</a>
                                      </li>                                      
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                      
                                      <div role="tabpanel" class="tab-pane active " id="tab_dataDiri" aria-labelledby="home-tab">
                                        <!-- start recent activity -->
                                        <ul class="messages">
                                          <div class=" item form-group">
                                            <label for="namaLengkap" class="col-form-label col-md-2 col-sm-2 label-align">Nama Lengkap</label>
                                                <input type="text" id="namaLengkap" name="namaLengkap" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> nama_lengkap}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="posisiDilamar" class="col-form-label col-md-2 col-sm-2 label-align">Posisi Dilamar</label>
                                                <input type="text" id="posisiDilamar" name="posisiDilamar" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> posisi_yang_dilamar}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="tempatLahir" class="col-form-label col-md-2 col-sm-2 label-align">Tempat Lahir</label>
                                                <input type="text" id="tempatLahir" name="tempatLahir" disabled class="form-control col-md-3 col-sm-3" value="{{$interview -> tempat_lahir}}">
                                            <label for="posisiDilamar" class="col-form-label col-md-3 label-align">Tanggal Lahir</label>
                                                <input type="text" id="posisiDilamar" name="posisiDilamar" disabled class="form-control col-md-3 col-sm-3" value="{{$interview -> tanggal_lahir}}">                                          
                                          </div>

                                          <div class=" item form-group">
                                            <label for="noKtp" class="col-form-label col-md-2 col-sm-2 label-align">No. KTP</label>
                                                <input type="text" id="noKtp" name="noKtp" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> no_ktp}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="jenisKelamin" class="col-form-label col-md-2 col-sm-2 label-align">Jenis Kelamin</label>
                                                <input type="text" id="jenisKelamin" name="jenisKelamin" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> jenis_kelamin}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="statusKawin" class="col-form-label col-md-2 col-sm-2 label-align">Status Perkawinan</label>
                                                <input type="text" id="statusKawin" name="statusKawin" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> status_perkawinan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="agama" class="col-form-label col-md-2 col-sm-2 label-align">Agama</label>
                                                <input type="text" id="agama" name="agama" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> agama}}">                                                
                                          </div>

                                          <div class=" item form-group">
                                            <label for="kebangsaan" class="col-form-label col-md-2 col-sm-2 label-align">Kebangsaan</label>
                                                <input type="text" id="kebangsaan" name="kebangsaan" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> kebangsaan}}">                                                
                                          </div>

                                          <div class=" item form-group">
                                            <label for="tanggalJoin" class="col-form-label col-md-2 col-sm-2 label-align">Tanggal Join</label>
                                                <input type="text" id="tanggalJoin" name="tanggalJoin" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> tanggal_join}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="npwp" class="col-form-label col-md-2 col-sm-2 label-align">NPWP</label>
                                                <input type="text" id="npwp" name="npwp" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> npwp}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="alamat" class="col-form-label col-md-2 col-sm-2 label-align">Alamat Domisili</label>
                                                <input type="text" id="alamat" name="alamat" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> alamat_domisili}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="alamatKtp" class="col-form-label col-md-2 col-sm-2 label-align">Alamat KTP</label>
                                                <input type="text" id="alamatKtp" name="alalamatKtpamat" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> alamat_ktp}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="telepon" class="col-form-label col-md-2 col-sm-2 label-align">Telepon</label>
                                                <input type="text" id="telepon" name="telepon" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> telepon}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="email" class="col-form-label col-md-2 col-sm-2 label-align">Email Pribadi</label>
                                                <input type="text" id="email" name="email" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> email}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="kendaraan" class="col-form-label col-md-2 col-sm-2 label-align">Jenis Kendaraan</label>
                                                <input type="text" id="kendaraan" name="kendaraan" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> kendaraan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="laptop" class="col-form-label col-md-2 col-sm-2 label-align">Memiliki Laptop?</label>
                                                <input type="text" id="laptop" name="laptop" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> laptop}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="harapGaji" class="col-form-label col-md-2 col-sm-2 label-align">Gaji Yang Diharapkan</label>
                                                <input type="text" id="harapGaji" name="harapGaji" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> gaji_diharap}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="capaianSudah" class="col-form-label col-md-2 col-sm-2 label-align">Capaian Yang Sudah</label>
                                                <input type="text" id="capaianSudah" name="capaianSudah" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> pencapaian_sudah_dilakukan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="capaianBelum" class="col-form-label col-md-2 col-sm-2 label-align">Capaian Yang Belum</label>
                                                <input type="text" id="capaianBelum" name="capaianBelum" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> pencapaian_yang_diinginkan}}">
                                          </div>                                          
                                        </ul>
                                        <!-- end recent activity -->
                                      </div>
                                      
                                      <div role="tabpanel" class="tab-pane fade" id="tab_dataKeluarga" aria-labelledby="profile-tab">
                                        <!-- start user projects -->
                                        <!-- Test Kepribadian -->
<div class="form-group">
  <label for="soal1">1. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test1" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test1" value="A" class="join-btn"> Saya adalah pekerja keras 
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test1" value="B" class="join-btn"> Saya tidak mudah murung
        </label>
      </div>      
    </div>
  </div>
</div>

<div class="form-group">
  <label for="soal2">2. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test2" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test2" value="A" class="join-btn"> Saya ingin bekerja lebih baik daripada orang lain
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test2" value="B" class="join-btn"> Saya ingin melaksanakan apa yang saya kerjakan sampai selesai
        </label>
      </div>      
    </div>
  </div>
</div>
<div class="form-group">
  <label for="soal3">3. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test3" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test3" value="A" class="join-btn"> Saya suka menunjukkan kepada orang lain bagaimana cara mengerjakan sesuatu
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test3" value="B" class="join-btn"> Saya ingin berusaha sebaik mungkin
        </label>
      </div>      
    </div>
  </div>
</div>
<div class="form-group">
  <label for="soal4">4. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test4" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test4" value="A" class="join-btn"> Saya melakukan hal-hal yang lucu
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test4" value="B" class="join-btn"> Saya suka mengatakan kepada orang lain apa yang harus dikerjakan
        </label>
      </div>      
    </div>
  </div>
</div>
<div class="form-group">
  <label for="soal5">5. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test5" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test5" value="A" class="join-btn"> Saya suka bergabung didalam kelompok
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test5" value="B" class="join-btn"> Saya ingin diperhatikan didalam kelompok
        </label>
      </div>      
    </div>
  </div>
</div>
<div class="form-group">
  <label for="soal6">6. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test6" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test6" value="A" class="join-btn"> Saya suka membuat persahabatan yang akrab
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test6" value="B" class="join-btn"> Saya suka berteman dalam suatu kelompok
        </label>
      </div>      
    </div>
  </div>
</div>
<div class="form-group">
  <label for="soal6">6. pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri Saudara</label>                                
  <div>
    <div id="test6" data-toggle="buttons">
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test6" value="A" class="join-btn"> Saya suka membuat persahabatan yang akrab
        </label>
      </div>
      <div>
        <label class="btn btn-outline-primary">
          <input type="radio" name="test6" value="B" class="join-btn"> Saya suka berteman dalam suatu kelompok
        </label>
      </div>      
    </div>
  </div>
</div>
                                        <!-- End Test Kepribadian -->

                                        <!-- end user projects -->
                                      </div>

                                      <div role="tabpanel" class="tab-pane fade" id="tab_riwayatKerja" aria-labelledby="profile-tab">
                                        <p>xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui
                                          photo booth letterpress, commodo enim craft beer mlkshk </p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
@endsection
