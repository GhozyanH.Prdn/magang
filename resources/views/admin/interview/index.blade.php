@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>PT Davinti Indonesia</h3>              
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Pelamar Akan Interview</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li> -->
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
          <a href="interview/trash" class="btn btn-custon-four btn-warning"><i class="fa fa-refresh fa-spin fa-fw"></i> Dapatkan Data Terhapus</a>
          @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Dihapus.
          </div>
          @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Pelamar</th>
            <th>Posisi Dilamar</th>
            <th>Telepon</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>


        <tbody>
          @foreach ($interview1 as $v => $interview )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$interview -> nama_lengkap}}</td>
            <td>{{$interview -> posisi_yang_dilamar}}</td>
            <td>{{$interview -> telepon}}</td>
            <td>
              @if($interview->tempat_lahir=='') Data Belum Lengkap
              @else Data Lengkap
              @endif             
            </td>
            <td>
                <a href="#" class="btn btn-custon-four btn-primary" 
                  data-myidintv="{{$interview -> id}}"
                  data-mynamaintv="{{$interview -> nama_lengkap}}"
                  data-myposisiintv="{{$interview -> posisi_yang_dilamar}}"
                  data-mycatatanintv="{{$interview -> catatan}}"
                  data-myalasanintv="{{$interview -> alasan}}"
                  data-toggle="modal" data-target="#modalKeputusan">Keputusan</a>
                <a href="#" class="btn btn-custon-four btn-success" 
                  data-myidintv="{{$interview -> id}}"
                  data-mynamaintv="{{$interview -> nama_lengkap}}"
                  data-myposisiintv="{{$interview -> posisi_yang_dilamar}}"
                  data-mycatatanintv="{{$interview -> catatan}}"
                  data-myalasanintv="{{$interview -> alasan}}"
                  data-toggle="modal" data-target="#modalCatatan">Catatan</a>
                <a href="/interview/{{$interview->id}}" class="btn btn-info">Detail</a>
                <form action="/interview/{{$interview->id}}" method="post" class="d-inline">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                </form>
            </td>
          </tr>
          @endforeach
        </tbody>

      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>


<div class="clearfix"></div>

<div class="col-md-12 col-sm-12  ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Pelamar Sudah Interview</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li> -->
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">

      <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
          
          <thead>
            <tr class="headings">
           
              <th>No </th>
              <th>Nama Pelamar</th>
              <th>Posisi Dilamar</th>
              <th>Telepon</th>
              <th>Status</th>
              <th>Aksi</th>
              
            </tr>
          </thead>

          <tbody>
            <tr class="even pointer">
            @foreach ($interview2 as $v => $interview )
              <td>{{$loop -> iteration}}</td>
              <td>{{$interview -> nama_lengkap}}</td>
              <td>{{$interview -> posisi_yang_dilamar}}</td>
              <td>{{$interview -> telepon}}</td>
              <td>
                @if($interview->status_lamaran=='Diterima') <a href="#" type="button" class="btn btn-primary btn-round"
                data-myidintv="{{$interview -> id}}"
                data-mynamaintv="{{$interview -> nama_lengkap}}"
                data-myposisiintv="{{$interview -> posisi_yang_dilamar}}"
                data-mykeputusanintv="{{$interview -> status_lamaran}}"
                data-mycatatanintv="{{$interview -> catatan}}"
                data-myalasanintv="{{$interview -> alasan}}"
                data-toggle="modal" data-target="#modalKeputusan2">Diterima </a>
                @elseif($interview->status_lamaran=='Ditolak : Good') <a href="#" type="button" class="btn btn-warning btn-round text-dark"
                data-myidintv="{{$interview -> id}}"
                data-mynamaintv="{{$interview -> nama_lengkap}}"
                data-myposisiintv="{{$interview -> posisi_yang_dilamar}}"
                data-mykeputusanintv="{{$interview -> status_lamaran}}"
                data-mycatatanintv="{{$interview -> catatan}}"
                data-myalasanintv="{{$interview -> alasan}}"
                data-toggle="modal" data-target="#modalKeputusan2">Ditolak : Good </a>
                @else <a href="#" type="button" class="btn btn-danger btn-round"
                data-myidintv="{{$interview -> id}}"
                data-mynamaintv="{{$interview -> nama_lengkap}}"
                data-myposisiintv="{{$interview -> posisi_yang_dilamar}}"
                data-mykeputusanintv="{{$interview -> status_lamaran}}"
                data-mycatatanintv="{{$interview -> catatan}}"
                data-myalasanintv="{{$interview -> alasan}}"
                data-toggle="modal" data-target="#modalKeputusan2">Ditolak : Bad </a>
                @endif     
              </td>
              <td>
                @if($interview->status_lamaran=='Ditolak : Bad')<a href="#" class="btn btn-info">Detail</a>
                <a href="#" class="btn btn-custon-four btn-warning">Ubah</a>
                <form action="#" method="post" class="d-inline">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                </form>
                @else <a href="#" class="btn btn-info">Detail</a>
                <a href="#" class="btn btn-custon-four btn-warning">Ubah</a>
                @endif
                
              </td>            
            </tr>
            @endforeach
          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<!-- --------- MODAL --------- -->
  <!-- --------- MODAL KEPUTUSAN --------- -->
  <div class="modal fade" id="modalKeputusan" tabindex="-1" role="dialog" aria-labelledby="modalTambahTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <form action="{{route('interview.update', 'test')}}" id="formKeputusan" method="POST">
      @method('patch')
      @csrf                
        <div class="modal-header">
          <h5 class="modal-title" id="modalTambahTitle">Keputusan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>                        
          <div class="modal-body" id="editKeputusan">
            <input type="hidden" id="idintv" name="idIntv" value="">

            <div class="form-group">
                  <label for="namaLengkap">Nama Lengkap</label>
                  <input type="text" id="namaintv" name="namaIntv" disabled class="form-control">
            </div>

            <div class="form-group">
                  <label for="posisisDilamar">Posisi Dilamar</label>
                  <input type="text" id="posisiintv" name="posisiIntv" disabled class="form-control">
            </div>
            
            <input type="hidden" id="catatanintv" name="catatanIntv" value="">

            <div class="form-group">
              <label for="keputusan">Keputusan</label>                                
              <div>
                <div id="keputusanintv" class="btn-group" data-toggle="buttons">
                  <label class="btn btn-primary">
                    <input type="radio" name="keputusanIntv" value="Diterima" class="join-btn"> Diterima 
                  </label>
                  <label class="btn btn-warning">
                    <input type="radio" name="keputusanIntv" value="Ditolak : Good" class="join-btn"> Ditolak : Good
                  </label>
                  <label class="btn btn-danger">
                    <input type="radio" name="keputusanIntv" value="Ditolak : Bad" class="join-btn"> Ditolak : Bad
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="alasanintv">Alasan Keputusan</label>
              <textarea id="alasanintv" name="alasanIntv" class="form-control"></textarea>
            </div>

          </div>                        
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" id = "closeModalTambah" >Tutup</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- --------- END MODAL KEPUTUSAN --------- -->

    <!-- --------- MODAL CATATAN --------- -->
    <div class="modal fade" id="modalCatatan" tabindex="-1" role="dialog" aria-labelledby="modalTambahTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <form action="{{route('interview.update', 'test')}}" id="formCatatan" method="POST">
        @method('patch')
        @csrf                 
        <div class="modal-header">
          <h5 class="modal-title" id="modalTambahTitle">Catatan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>                        
          <div class="modal-body" id="editCatatan">
            <input type="hidden" id="idintv" name="idIntv" value="">

            <div class="form-group">
                  <label for="namaLengkap">Nama Lengkap</label>
                  <input type="text" id="namaintv" name="namaIntv" disabled class="form-control">
            </div>

            <div class="form-group">
                  <label for="posisisDilamar">Posisi Dilamar</label>
                  <input type="text" id="posisiintv" name="posisiIntv" disabled class="form-control">
            </div>

            <div class="form-group">
              <label for="catatanintv">Catatan Interview</label>
              <textarea id="catatanintv" name="catatanIntv" class="form-control"></textarea>
            </div>

            <input type="hidden" id="keputusanintv" name="keputusanIntv" value="">
            
            <input type="hidden" id="alasanintv" name="alasanIntv" value="">
            
          </div>                        
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" id = "closeModalTambah" >Tutup</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- --------- END MODAL CATATAN --------- -->

  <!-- --------- MODAL KEPUTUSAN2 --------- -->
  <div class="modal fade" id="modalKeputusan2" tabindex="-1" role="dialog" aria-labelledby="modalTambahTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <form action="{{route('interview.update', 'test')}}" id="formKeputusan2" method="POST">
      @method('patch')
      @csrf                
        <div class="modal-header">
          <h5 class="modal-title" id="modalTambahTitle">Keputusan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>                        
          <div class="modal-body" id="editKeputusan2">
            <input type="hidden" id="idintv" name="idIntv" value="">

            <div class="form-group">
                  <label for="namaLengkap">Nama Lengkap</label>
                  <input type="text" id="namaintv" name="namaIntv" disabled class="form-control">
            </div>

            <div class="form-group">
                  <label for="posisisDilamar">Posisi Dilamar</label>
                  <input type="text" id="posisiintv" name="posisiIntv" disabled class="form-control">
            </div>

            <div class="form-group">
              <label for="keputusan">Keputusan</label>                                
              <div>
                <div id="keputusanintv" class="btn-group" data-toggle="buttons">
                  <label class="btn btn-primary">
                    <input type="radio" name="keputusanIntv" value="Diterima" class="join-btn"> Diterima 
                  </label>
                  <label class="btn btn-warning">
                    <input type="radio" name="keputusanIntv" value="Ditolak : Good" class="join-btn"> Ditolak : Good
                  </label>
                  <label class="btn btn-danger">
                    <input type="radio" name="keputusanIntv" value="Ditolak : Bad" class="join-btn"> Ditolak : Bad
                  </label>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="alasanintv">Alasan Keputusan</label>
              <textarea id="alasanintv" name="alasanIntv" class="form-control"></textarea>
            </div>

            <div class="form-group">
              <label for="catatanintv">Catatan Interview</label>
              <textarea id="catatanintv" name="catatanIntv" class="form-control"></textarea>
            </div>

          </div>                        
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" id = "closeModalTambah" >Tutup</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- --------- END MODAL KEPUTUSAN2 --------- -->
@endsection