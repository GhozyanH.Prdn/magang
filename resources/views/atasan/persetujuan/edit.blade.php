@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Edited <small>By : {{auth()->user()->level}} ({{auth()->user()->nama}})</small></h3>
              </div>
            </div>  
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Aplikasi Aset  <small>PT Davinti Indonesia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
         
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form action="/persetujuan_atasan/{{$cuti->id}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
          @method('patch')
          @csrf
         
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Tanggal Request Cuti <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="tanggal_request" value="{{$cuti -> tanggal_request}}" readonly class="form-control" >
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nama Karyawan</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$cuti ->employee-> nama}}"  readonly type="text" name="nama_karyawan">
            </div>
          </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Nama Atasan <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="nama_atasan" value="{{$cuti -> employee-> nama_atasan}}" readonly class="form-control" >
            </div>
          </div>
          
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Tanggal Cuti <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="tanggal_cuti" value="{{$cuti -> tanggal_cuti}}" readonly class="form-control" >
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Lama Cuti</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$cuti ->lama_cuti}}"  readonly type="text" name="lama_cuti">
            </div>
          </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Alasan Mengambil Cuti <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
            <textarea class="form-control"   rows="3"  readonly type="text" name="alasan_cuti">{{$cuti ->alasan}}</textarea>
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Cuti Yang Sudah Terpakai</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$cuti -> cuti_terpakai}}" readonly type="text" name="cuti_terpakai">
            </div>
          </div>

  
          <!-- peruangan keputusan atasan -->
          @php $button; @endphp  
          @if($cuti -> keputusan_direktur  == 'menunggu' )  @php $button='btn btn-warning btn-lg'; @endphp
          @elseif ($cuti -> keputusan_direktur == 'diterima' ) @php $button='btn btn-success btn-lg'; @endphp
          @else  @php $button='btn btn-danger btn-lg'; @endphp
          @endif
          @php $data=$button @endphp

            <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"> Keputusan Direktur</label>
            <div class="col-md-3 col-sm-3 ">
            <button type="button" class="{{$data}}">{{$cuti -> keputusan_direktur}}</button>
            </div>
 

       
          <!-- peruangan keputusan atasan -->
          @php $button; @endphp  
          @if($cuti -> keputusan_hrd  == 'menunggu' )  @php $button='btn btn-warning btn-lg'; @endphp
          @elseif ($cuti -> keputusan_hrd == 'diterima' ) @php $button='btn btn-success btn-lg'; @endphp
          @else  @php $button='btn btn-danger btn-lg'; @endphp
          @endif
          @php $data=$button @endphp
            <label for="middle-name" class="col-form-label col-md-1 col-sm-1 label-align"> Keputusan HRD</label>
            <div class="col-md-3 col-sm-3 ">
            <button type="button" class="{{$data}}">{{$cuti -> keputusan_hrd}}</button>
            </div>
            </div>

            <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Keputusan Anda <span class="required">*</span>
            </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select name="keputusan_anda" class="form-control">
                            <option>Pilih Opsi</option>
                            <option>diterima</option>
                            <option>ditolak</option>
                          </select>
                        </div>
                      </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Catatan Anda <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
            <textarea class="form-control"  value="" rows="3"  type="text" name="catatan_anda"></textarea>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="item form-group ">
            <div class="col-md-6 col-sm-6 offset-md-3 ">
              <button type="reset" class="btn btn-round btn-primary">Reset</button>
              <button type="submit" class="btn btn-round btn-success">Save</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>  

@endsection
