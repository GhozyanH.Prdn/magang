<?php

namespace App;
use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Employee extends Authenticatable
{
    
    public function cuti(){  
        return $this ->hasOne('App\Cuti','id_karyawan') ;
      }
      public function postulant(){
        return $this ->belongsTo('App\Postulant','id_postulant');
        }
      public static function getId(){
        return $getId = DB::table('cuti_categorys')->orderBy('id','DESC')->take(1)->get();
      }
    
    
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = "employees";
     protected $primaryKey = "id";

    protected $fillable = ['name', 'kode_karyawan', 'password','level'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
