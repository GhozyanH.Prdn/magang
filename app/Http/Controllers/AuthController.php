<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
  public function formlogin(){
    return view ('formlogin');
  }

  public function formregister(){
    return view ('formregister');
  }

  public function postlogin(Request $request){
    if(Auth::attempt($request->only('kode_karyawan','password'))){
      return redirect('admin');
    }
    return redirect('formlogin');
  }

  public function logout(){
    Auth::logout();
    return redirect('formlogin');
  }
}
