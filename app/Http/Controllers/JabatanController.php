<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::all();
        return view ('admin/jabatan.index',compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
    $request->validate([
        'namaJab' => 'required'
    ]);

    $id = DB::table('positions')->orderBy('id','DESC')->take(1)->get();
    foreach ($id as $value);
    $idlama = $value->id;
    $idbaru = $idlama + 1;
    $kode_jabatan = 'Pos-'.$idbaru;

    Position::create([
        'kode_jabatan' => $kode_jabatan,
        'nama_jabatan' => $request->namaJab,
    ]);
    //dd($request);    
    return redirect('/jabatan')-> with('status', 'Data Jabatan Baru Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Position  $division
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Position  $division
     * @return \Illuminate\Http\Response
     */
    public function edit(Position $position)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Position  $division
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());
        
        $request->validate([
            'namaJab' => 'required'
        ]);

        Position::where('id',$request->idJab)
        ->update([
            'nama_jabatan' => $request->namaJab
        ]);
        
        return redirect('/jabatan')-> with('edit', 'Data Jabatan Berhasil Diubah !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Position  $division
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Position $position)
    {   
        Position::destroy($position->id);
        return redirect('/jabatan')-> with('delete', 'Data Aset Berhasil Dihapus');
    }

    public function trash(){
        $jabatan = Position::onlyTrashed()->get();
        return view ('admin.jabatan.trash', ['positions' => $jabatan]);
      }

    public function restore($id){
        $jabatan = Position::onlyTrashed()->where('id', $id);
        $jabatan->restore();
        return redirect('/jabatan/trash')-> with('restore', 'Data Position Berhasil di Restore');
      }
  
    public function deleted_permanent($id){
        $jabatan = Position::onlyTrashed()->where('id', $id);
        $jabatan->forceDelete();
  
        return redirect('/jabatan/trash')-> with('delete', 'Data Position Berhasil di Delete Permanent');
      }
  
    public function restore_all(){
        $jabatan = Position::onlyTrashed();
        $jabatan->restore();
  
        return redirect('/jabatan/trash')-> with('restore_all', 'Data Position Berhasil di Restore all');
      }
  
    public function deleted_all(){
        $jabatan = Position::onlyTrashed();
        $jabatan->forceDelete();
  
        return redirect('/jabatan/trash')-> with('deleted_all', 'Data Position Berhasil di Delete Semua');
      }
}
