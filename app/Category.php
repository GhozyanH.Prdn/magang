<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = "cuti_categorys";
    protected $fillable = ['kode_kategori_cuti', 'jenis_cuti', 'keterangan', 'value'];
    
    public function cuti(){  
        return $this ->hasOne('App\Cuti','id_cuti') ;
      }
}
