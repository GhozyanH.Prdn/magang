<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class AssetTransaction extends Model
{


    protected $fillable = ['kode_peminjaman', 'id_aset', 'id_pegawai', 'tanggal_pinjam', 'tanggal_kembali'];

    public static function getId(){
     return $getId = DB::table('asset_transactions')->orderBy('id','DESC')->take(1)->get();
 }
 	public function gabung(){
		return $this ->belongsTo('App\Asset','id_aset');
    }
}
